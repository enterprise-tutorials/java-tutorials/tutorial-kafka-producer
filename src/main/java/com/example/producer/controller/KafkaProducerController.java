package com.example.producer.controller;

/*
 * Copyright (C) 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import com.example.producer.model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.atomic.AtomicLong;

/**
 * The Controller for Producer.
 * 
 * @author michael
 * @since 24 Nov 2018
 */

@Controller
public class KafkaProducerController {

    private static final String template = "Kafka Producer says Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/helloProducer")
    @ResponseBody
    public User consumerHello(@RequestParam(name = "name", required = false, defaultValue = "Stranger") String name) {
        return new User(counter.incrementAndGet(), String.format(template, name));
    }

}
